- name: Secure and Protect, Secure - Section MAU, SMAU - Unique users who have used a Secure scanner
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The number of unique users who have run one or more Secure scanners.
  target: Progressively increasing month-over-month
  org: Secure and Defend Section
  section: securedefend
  stage: secure
  public: true
  pi_type: Section MAU, SMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Instrumentation
    reasons:
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Threat Insights data is not yet included in this count as it just moved from
      Defend.
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9282766
    dashboard: 707777
    embed: v2
  sisense_data_secondary:
    chart: 9926446
    dashboard: 707777
    embed: v2
- name: Secure:Static Analysis - GMAU - Users running SAST
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more SAST jobs.
  target: Progressively increasing month-over-month, >10%
  org: Secure and Defend Section
  section: securedefend
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Instrumentation
    reasons:
    - Currently missing self-managed instrumentation.
    - Can't confidently differentiate between Core and Ultimate users. [WIP exploration](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin?widget=9573161&udv=1090066)
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
    - Known [issue for timeouts](https://gitlab.com/gitlab-org/gitlab/-/issues/233610) causing some data to not report.
  lessons:
    learned:
      - Tracking correlation between in app clicks and upgrades is extremely difficult with existing product analytics system. Will have to work closely with Telemetry and Growth teams to successfully track SAST Core usage upgrades
      - SAST to Core continues to have no effect on downgrades thus far. [Upcoming core experience improvements](https://gitlab.com/groups/gitlab-org/-/epics/4388) should further increase adoption and usage rates.
      - Beginning to see leveling off of growth spike from SAST to Core and inclusion in Auto DevOps. Future feature additions should keep growth going. More messaging and enablement coming soon.
      - We're likely still undercounting this as we use an exact match on SAST job names rather than ilike fuzzy match for SAST which will catch more job names. This is being updated. 
  monthly_focus:
    goals:
      - Now that SAST is availible to all plan types, creating a better experience for non-Ultimate users to help them discover value and become interested in upgrading. [Improved MR experience](https://gitlab.com/groups/gitlab-org/-/epics/4388) & [Improved Configuration Experience](https://gitlab.com/gitlab-org/gitlab/-/issues/241377).
      - Delivering [MVC of SAST Custom Rulesets](https://gitlab.com/groups/gitlab-org/-/epics/4179), our most requested feature, to drive paid adoption and extensibility of SAST. 
      - Major [rewrite of NodeJS SAST scanner](https://gitlab.com/gitlab-org/gitlab/-/issues/220847) to add 100+ new rules which will increase coverage and improve accuracy. 
      - Adding highly requested [iOS & Android SAST scanning](https://gitlab.com/gitlab-org/gitlab/-/issues/233777) to drive additional usage and adoption of the Secure stage. 
      - We're wrapping up our [MVC SAST Configuration UI](https://gitlab.com/groups/gitlab-org/-/epics/3262) which now supports existing configurations and specific analyzer settings to make it easier to get started with SAST.
  metric_name: user_sast_jobs, user_secret_detection_jobs
  sisense_data:
    chart: 9284778
    dashboard: 707777
    embed: v2
  sisense_data_secondary:
    chart: 9926456
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Static Analysis - GMAU - Users running Secret Detection
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Secret Detection jobs.
  target: Progressively increasing month-over-month, >10%
  org: Secure and Defend Section
  section: securedefend
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Instrumentation
    reasons:
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
    - Known [issue for timeouts](https://gitlab.com/gitlab-org/gitlab/-/issues/233610) causing some data to not report.
    - Can't confidently differentiate between Core and Ultimate users. [WIP exploration](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin?widget=9430199&udv=1090066)
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - Begining to see leveling off of growth spike from Secret Detection to Core and inclusion in Auto DevOps. Future feature additions should keep growth going.
      - We're likely still undercounting this as we use an exact match on secret-detection job names rather than ilike fuzzy match for secret-detection which will catch more job names. This is being updated. 
      - Secret Detection to Core had no effect on downgrades thus far however it has not significantly driven Gold Adoption in the same way SAST To Core has. Customers want more from Secret Detection like [auto-remediation](https://gitlab.com/gitlab-org/gitlab/-/issues/10047), which we are actively considering for the roadmap and [potential tie in with Secrets Management](https://gitlab.com/gitlab-org/gitlab/-/issues/216276). 
  monthly_focus:
    goals:
      - Now that Secret Detection is available to all plan types, creating a better experience for non-Ultimate users to help them discover value and become interested in upgrading. [Improved MR experience](https://gitlab.com/groups/gitlab-org/-/epics/4388) & [Improved Configuration Experience](https://gitlab.com/gitlab-org/gitlab/-/issues/241377).
      - Delivering [MVC of Secret Detection Custom Rulesets](https://gitlab.com/groups/gitlab-org/-/epics/4179), our most requested feature, to drive paid adoption and extensibility of Secret Detection. 
      - Starting design phase for [Configuration UI for Secret Detection](https://gitlab.com/groups/gitlab-org/-/epics/4496) to simplify enablement and drive adoption.  
  metric_name: user_secret_detection_jobs
  sisense_data:
    chart: 9620614
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Dynamic Analysis - GMAU - Users running DAST
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: Number of unique users who have run one or more DAST jobs.
  target: Progressively increasing month-over-month
  org: Secure and Defend Section
  section: securedefend
  stage: secure
  group: dynamic_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Steady monthly increase over the last two months
  implementation:
    status: Instrumentation
    reasons:
    - Instrumentation in place.
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
  lessons:
    learned:
      - On-demand scans driving MAU increase to above where it was in May
      - On-demand scans significantly increased the number of projects DAST is used in
      - More profile options need to be introduced for on-demand DAST scans to have broad usage
  monthly_focus:
    goals:
      - Add more options to Scanner profile (https://gitlab.com/gitlab-org/gitlab/-/issues/225804)
      - Add more options to Site profile (https://gitlab.com/groups/gitlab-org/-/epics/3771)
      - DAST Site validation (https://gitlab.com/gitlab-org/gitlab/-/issues/233020)
  metric_name: user_dast_jobs
  sisense_data:
    chart: 9620496
    dashboard: 697792
    embed: v2
  sisense_data_secondary:
    chart: 9926656
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Composition Analysis - GMAU - Users running any SCA scanners
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Container
    Scanning jobs, number of unique users who have run one or more Dependency Scanning
    jobs, and number of unique users who have run one or more License Scanning jobs.
  target: Progressively increasing month-over-month, >2%
  org: Secure and Defend Section
  section: securedefend
  stage: secure
  group: composition_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Instrumentation
    reasons:
    - Unclear provenance and lineage of data, trying to make PI charts and improve MAU charts but having large data discrepancy and lack of documentation issues.
    - All data in these charts should be used to indicate trending and should not be taken in any way as accurate numbers. Data counts between various databases tables show wild inconsistency on count, although trend nearly identically.
    - We are unable, with many tables, to tell which data should be removed and attributed to GitLab team members.
    - There is some difficulty in locking down what are accurate current licenses for data.
    - We have timeout issues with the data database (known issue) and are having problems counting at least [License Compliance](https://gitlab.com/gitlab-org/gitlab/-/issues/202124) when using [this model](https://gitlab.com/gitlab-org/gitlab/-/issues/211621).
    - We may be sending [too much data for the data team to handle](https://gitlab.com/gitlab-org/gitlab/-/issues/219334) and have been asked to rethink that.
    - We only count successful job runs not [failed](https://gitlab.com/gitlab-org/gitlab/-/issues/241201) ones in usage ping, need to investigate analytics.gitlab_dotcom_secure_stage_ci_jobs as it may conditionally provide some failure data.
    - Can't differentiate [between the same user running multiple job types](https://gitlab.com/gitlab-org/gitlab/-/issues/230982), so possible
      double counting.
    - Chart is for .com-only data while usage ping becomes more stable for on-premise. [Issue1](https://gitlab.com/gitlab-org/gitlab/-/issues/229618#note_381037115) and [Issue2](https://gitlab.com/gitlab-org/gitlab/-/issues/231263).
    - Can't currently [differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users](https://gitlab.com/gitlab-org/gitlab/-/issues/230981).
  lessons:
    learned:
      - September: Nothing new learned but more concerns added above to the instrumentation area.
      - Some observations for SaaS
        - 8% of the paid utilization is from paid accounts, but not gold accounts, and there are ~2.5x times free users than paid users (by virtue of having public projects). 
        - Dependency Scanning maintains being the most popular with 76% of the MAU. 
        - Dependency Scanning had a large temporary bump in scans run, but not in total projects, in June I have not been able to pin down yet.
  monthly_focus:
    goals:
      - October: I would like to find out where some of the data comes for as none of it is consistent. Also, same as last month, continuing to try and move forward with our first [SaaS PI - number of findings](https://gitlab.com/groups/gitlab-org/-/epics/3952) which we have been working for 4+ releases.
  metric_name: user_container_scanning_jobs, user_license_management_jobs, user_dependency_scanning_jobs
  sisense_data:
    chart: 9822707
    dashboard: 749790
    embed: v2
  sisense_data_secondary:
    chart: 9926775
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Fuzz Testing - GMAU - Users running fuzz testing
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Fuzz
    Testing jobs.
  target: Progressively increasing month-over-month, >25%
  org: Secure and Defend Section
  section: securedefend
  stage: secure
  group: fuzz_testing
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - We have a good growth rate and need more users to use fuzz testing.
  implementation: 
    status: Instrumentation
    reasons:
    - We now have reporting on .com.
    - Self-managed likely to capture no results due to being keyed off of job name rather than job type. Tracking work on improving this in [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239118).
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - We learned how to use a new source for reporting .com usage until we can report on job artifacts. This data appears accurate so may not need to move off it for .com.
      - We learned about changes needed to usage ping. Since users can set their own job names, we can't look just at job names.
      - We observed .com Snowplow working correctly, as we had several users reporting data.
      - Self-managed likely to capture no results due to being keyed off of job name rather than job type. Tracking work on improving this in [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239118).
  monthly_focus:
    goals:
      - Fuzz testing results in MR widget (https://gitlab.com/gitlab-org/gitlab/-/issues/210343)
      - Investigation for how to support Java Spring apps (https://gitlab.com/gitlab-org/gitlab/-/issues/254654)
      - API fuzz testing foundational work to use Security Dashboard (https://gitlab.com/gitlab-org/gitlab/-/issues/224726)
      - Re-symbolicate fuzz crash results (https://gitlab.com/gitlab-org/gitlab/-/issues/224518)
  metric_name: user_coverage_fuzzing_jobs
  sisense_data:
    chart: 9842394
    dashboard: 707777
    embed: v2
  sisense_data_secondary:
    chart: 9926832
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Threat Insights - GMAU - Users interacting with Secure UI
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: Number of unique sessions viewing a security dashboard, pipeline security
    report, or expanding MR security report. Same as Paid until OSS projects
    can be separated.
  target: 10% month-over-month increase
  org: Secure and Defend Section
  section: securedefend
  stage: secure
  group: threat_insights
  public: true
  pi_type: GMAU
  product_analytics_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - Data numbers and trends may be misrepresented (see below)
  implementation:
    status: Instrumentation
    reasons:
    - Recent deep-dive into data indicates page view usage queries may not be accurate
    - GitLab employee usage may be greatly overcounted for certain feature pages
    - May require new data instrumentation to correct
    - Collection is complete for .com only
    - Snowplow data is session only, not unique users
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Cannot separate out GitLab employee usage from customers
  lessons:
    learned:
      - Page URL pattern matching may be insufficiently deterministic to support accurate GMAU
      - While not part of GMAU, overall unique vulnerability record views are up 90% from August
  monthly_focus:
    goals:
      - Work with Data team to resolve current data issues and/or formulate new metrics capture plan: https://gitlab.com/gitlab-data/analytics/-/issues/6404
      - Adding new Vulnerability Trends chart as foundation for project-level dashboard (https://gitlab.com/gitlab-org/gitlab/-/issues/235558)
  metric_name: TBD
  sisense_data:
    chart: 9244137
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Protect, Protect:Container Security - SMAU, GMAU - Users running Container Scanning or interacting with Protect UI
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The number of active clusters with at least one Container Security feature
    enabled (WAF, Network Policies, or Host Security) in the last 28 days.
  target: Progressively increasing month-over-month, >2%
  org: Secure and Defend Section
  section: securedefend
  stage: Defend
  group: container_security
  public: true
  pi_type: SMAU, GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - On track
  implementation:
    status: Instrumentation
    reasons:
    - Limitations in data (see SCA limitations above)
    - Data is not available yet for the Security Orchestration category
  lessons:
    learned:
      - TBD
  monthly_focus:
    goals:
      - Allow container security scans to be run against containers in production environments (https://gitlab.com/groups/gitlab-org/-/epics/3410)
  metric_name: user_container_scanning_jobs
  sisense_data:
    chart: 9930861
    dashboard: 694854
    embed: v2
- name: Protect, Protect:Container Security - SMAC, GMAC - Clusters using Container Network or Host Security
  base_path: "/handbook/product/secure-and-protect-section-performance-indicators/"
  definition: The number of active clusters with at least one Container Security feature
    enabled (Network Policies, or Host Security) in the last 28 days.
  target: Progressively increasing >25% month-over-month
  org: Secure and Defend Section
  section: securedefend
  stage: Defend
  group: container_security
  public: true
  pi_type: SMAC, GMAC
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - Due to the delay in getting metrics, it is still too early to assess usage levels or trends.  The August data is incomplete.  September's data is complete; however, we will need one more month of complete data (October) to assess trends.
  implementation:
    status: Complete
    reasons:
    - GMAC data is now available for Container Network Security; however, due to the 51 day reporting delay, it is still too early to assess usage levels or trends
    - Data collection for CHS is [planned but not yet implemented](https://gitlab.com/gitlab-org/gitlab/-/issues/218800) as the category is relatively new
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - We have 7 clusters using CNS, all of which are on gitlab.com.  Customers are using the blocking mode as some traffic is being dropped.
      - Although it is still too early to establish trends (growth or decline) we now have a good usage baseline.
  monthly_focus:
    goals:
      - Alert Dashboard MVC (https://gitlab.com/groups/gitlab-org/-/epics/3438)
  metric_name: user_container_scanning_jobs
  sisense_data:
    chart: 9620410
    dashboard: 694854
    embed: v2
